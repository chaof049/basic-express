// CRUD = Create Read Update Delete
// used as post,get,patch,delete method
// backend = it makes an api used for defining task for each request
// postman = used to hit and test api
// expressApp.use(json()); =》 always place this code at the top of the router
// if you send data from body always use post or patch method
// url = localhost:8000/student?name=chao&age=33
// url = route + query
// route = localhost:8000/student (student = route parameter)
// route parameter is divided into two parts
// one is static route parameter (must use same name as mentioned)
// and the other is dynamic route parameter (can use any name determined by '/:(any word)')
// always place those api which has dynamic route parameter at last
// query = name=chao&age=33 (name and age = query parameter)
// whatever we pass in query, everything returns in string
// send data from postman
// body = req.body
// query = req.query
// params  = req.params
// one request can obtain only one response
// middleware are the functions which has three parameters (req, res and next) = (normal middleware)
// to trigger next middleware, we have to call next()
// there are two middleware, normal and error. 
// error middleware has four parameter (err, req, res, next)
// to trigger error middleware, we have to call next('err data')

