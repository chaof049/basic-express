import { Router } from "express";

let vehicleRouter = new Router();

vehicleRouter
  .route("/")
  .post((req,res) => {
    res.json({ success: true, message: "vehicles created successfully" });
  })
  .get((req,res) => {
    res.json({ success: true, message: "vehicles read successfully" });
  })
  .patch((req,res) => {
    res.json({ success: true, message: "vehicles updated successfully" });
  })
  .delete((req,res) => {
    res.json({ success: true, message: "vehicles deleted successfully" });
  });

export default vehicleRouter;
