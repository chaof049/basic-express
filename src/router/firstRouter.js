import { Router } from "express";

let firstRouter = Router();

firstRouter
.route("/") // while selecting api, it only sees the route and doesn't see the url
.post((req,res) => {
    console.log('body data',req.body)
    console.log('query data',req.query)
  res.json("home post")
//   res.json('this response is invalid') // one request gets one response
})

firstRouter
  .route("/name")
  .post((req,res) => {
    res.json("name post");
  })

firstRouter
.route('/a/:city/b/:name')
.post((req,res)=>{
    console.log(req.params)
    res.json('yo')
})
export default firstRouter;
