import { Router } from "express";

let traineeRouter = new Router()

traineeRouter
  .route("/")
  .post((req, res) => {
    res.json({ success: true, message: "school created successfully" });
  })
  .get((req, res) => {
    res.json({ success: true, message: "school read successfully" });
  })
  .patch((req,res)=>{
    res.json({ success: true, message: "school updated successfully" });
  })
  .delete((req,res)=>{
    res.json({ success: true, message: "school deleted successfully" });
  })

export default traineeRouter