// make an express application
// assign a port to that application

import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import vehicleRouter from "./src/router/vehicleRouter.js";

let expressApp = express();
expressApp.use(json()); // always place this code at the top of the router

let port = 8000;
expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});

// middleware
/* expressApp.use(
  (req,res,next)=>{
    console.log('app middleware1')
    next()
  },
  (req,res,next)=>{
    console.log('app middleware2')
    next()
  },
  (req,res,next)=>{
    console.log('app middleware3')
    next()
  }
) */

expressApp.use("/", firstRouter);
expressApp.use("/bike", bikeRouter);
expressApp.use("/trainee", traineeRouter);
expressApp.use("/vehicle", vehicleRouter);
